import { Component, OnInit } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/domain/entity/User';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.sass']
    
})
export class UsersComponent implements OnInit {
    user: User = new User('', '', undefined, undefined, '');

    get properties(): string[] {
        return Object.keys(this.user);
    }

    // Here I am handling displayedColumns dynamically.  
    displayedColumns: string[] = this.properties.filter(prop => prop !== 'id');
    users$: Observable<User[]> = EMPTY;

    constructor(
        private readonly userService: UserService
    ) { }

    ngOnInit(): void {
        console.log(this.displayedColumns);
        this.users$ = this.userService.getAllUsers()
    }
}
