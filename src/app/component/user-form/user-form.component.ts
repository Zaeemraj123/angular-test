import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, lastValueFrom, of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/domain/entity/User';
import { AgeEstimator__ } from 'src/domain/service/AgeEstimator';
import { GenderEstimator__ } from 'src/domain/service/GenderEstimator';
import { NationalityEstimator__ } from 'src/domain/service/NationalityEstimator';
import { Estimator } from 'src/domain/service/estimator';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.sass']
})
export class UserFormComponent implements OnInit {

    constructor(
        private userService: UserService,
        private snackBar: MatSnackBar,
        public nationalityEstimator: NationalityEstimator__,
        public ageEstimator: AgeEstimator__,
        public genderEstimator: GenderEstimator__,
        ) { }
    
    estimators: Estimator[] = [
        this.ageEstimator,
        this.genderEstimator,
        this.nationalityEstimator,
      ];

    nameControl = new FormControl()
    user?: User

    userForm = new FormGroup({
        name: this.nameControl
    })

    ngOnInit(): void {
        this.nameControl.valueChanges.subscribe(newValue => this.onNameChanged(newValue))
    }

    onSubmit() {
        if (this.user) {
            this.userService.saveUser(this.user).subscribe()
            this.nameControl.setValue("")
            this.user = undefined
        }
    }

    async onNameChanged(newName: string) {
        try {

            if (newName.length < 4) {
                this.snackBar.open("Name should be atleast 3 characters long",undefined,{duration:1000});
                return
            }

            // Here I had implemented Open and Closed Principle

            this.user = User.createNew(newName)
            const requests = this.estimators.map(estimator => estimator.estimate(this.user));
            
            const promises = requests.map(request => lastValueFrom(request));
            Promise.all(promises).then(responses => {
                console.log(responses);
            });

            console.log(this.user);

        } catch (error) {
            console.log(error);
        }
    }
}
