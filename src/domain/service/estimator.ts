import { Observable } from "rxjs";
import { User } from "../entity/User";

export interface Estimator {
    estimate(user?: User): Observable<User>;
}
