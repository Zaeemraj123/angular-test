import { Observable, catchError, concatMap, of, throwError } from "rxjs";
import { User } from "../entity/User";
import { Injectable } from "@angular/core";
import { Estimator } from "./estimator";

@Injectable({
    providedIn: 'root'
})
export abstract class NationalityEstimator {
    abstract estimateNationalityForUser(user: User): Observable<string>;
}


@Injectable()
export class NationalityEstimator__ implements Estimator {
    constructor(
        private nationalityEstimator: NationalityEstimator
    ) { }
    
    estimate(user: User): Observable<any> {
        return this.nationalityEstimator.estimateNationalityForUser(user).pipe(concatMap(country => {
            user.country = country
            return of(country)
        })).pipe(catchError(error => {
            console.log('Error occurred:', error);
            return throwError('Failed to estimate nationality');
        }));
    }
}

