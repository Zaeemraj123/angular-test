import { Observable, concatMap, of } from "rxjs";
import { User } from "../entity/User";
import { Injectable } from "@angular/core";
import { Estimator } from "./estimator";

@Injectable({
    providedIn: 'root'
})
export abstract class AgeEstimator {
    abstract estimateAgeForUser(user: User): Observable<number>;
}



@Injectable()
export class AgeEstimator__ implements Estimator {
    constructor(
        private ageEstimator: AgeEstimator,
    ) { }
    estimate(user: User): Observable<any> {
        return this.ageEstimator.estimateAgeForUser(user).pipe(concatMap(age => {
            user.age = age
            return of(age)
        }))
    }
}