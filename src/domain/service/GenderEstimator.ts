import { Observable, concatMap, of } from "rxjs";
import { User } from "../entity/User";
import { Injectable } from "@angular/core";
import { Estimator } from "./estimator";

@Injectable({
    providedIn: 'root'
})
export abstract class GenderEstimator {
    abstract estimateGenderForUser(user: User): Observable<string>;
}

@Injectable()
export class GenderEstimator__ implements Estimator {
    constructor(
        private genderEstimator: GenderEstimator,
    ) { }
    
    estimate(user: User): Observable<any> {
        return this.genderEstimator.estimateGenderForUser(user).pipe(concatMap(gender => {
            user.gender = gender
            return of(gender)
        }))
    }
}